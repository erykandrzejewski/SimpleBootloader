ASM = nasm
ASM_PARAMS = -Isrc/include/

BINARIES = bin/stage1.bin bin/stage2.bin

all: image.bin

image.bin: $(BINARIES)
	cat $^ > $@

bin/%.bin: src/%.asm
	$(ASM) $(ASM_PARAMS) -o $@ $<

clean:
	rm bin/*
	rm image.bin

run:
	qemu-system-x86_64 -hda image.bin

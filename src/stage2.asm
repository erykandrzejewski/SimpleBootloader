[BITS 16]
[ORG 0x7E00]

stage2_start:
	; Set up segment registers again
	xor ax, ax
	mov ds, ax ; DS = 0
	mov es, ax ; ES = 0

	mov si, msg_stage2_loaded
	call print_string

	; A20 unlocking process

	call check_a20
	jnz .end_a20

	call unlock_a20_by_interrupt
	call check_a20
	jnz .end_a20

	mov si, msg_couldnt_unlock_a20
	call print_string

	cli
	hlt
	jmp $

.end_a20:

	; Loading GDT
	cli
	lgdt [gdt_info]

	; Now we can get into 32 bit protected mode :)

	mov eax, cr0
	or eax, 1 ; We set bit 0
	mov cr0, eax

	jmp dword 0x8:start_32bit

	hlt
	jmp $


%include "rmode_screen.asm"
%include "a20.asm"
%include "gdt.asm"

msg_stage2_loaded db "Stage 2 has been successfully loaded.", 0xA, 0xD, 0
msg_couldnt_unlock_a20 db "Could not unlock A20 gate, sorry.", 0

; Protected mode
start_32bit:
	[BITS 32]

	cli
	mov ax, 0x10 ;Data selector

	mov ds, ax
	mov es, ax
	mov ss, ax
	mov gs, ax
	mov fs, ax

	mov ebx, 0xB8000
	mov word [ebx], 0x4545

	cli
	hlt
	jmp $

times 512 - ($ - $$) db 0

[BITS 16]
[ORG 0x7C00]

jmp word 0x0000:bootloader_start

bootloader_start:

	; Set up data segment registers
	xor ax, ax
	mov ds, ax ; DS = 0
	mov ax, 0x7E0
	mov es, ax ; ES = 0x7E0 (temporary, on 0x07E0:0x0000 = 0x7E00 will be loaded next sectors)

	; Set up stack
	mov ax, 0x6BF
	mov ss, ax
	mov sp, 0x1000 ; size 4kB

	call clear_screen

	mov si, msg_welcome
	call print_string

	; Read sectors
	mov ah, 2 ; Read sectors into memory
	mov al, 1 ; Sectors amount
	mov ch, 0 ; Cylinder number - low eight bits
	mov cl, 2 ; First sector number 1-63 (bits 0-5)
	mov dh, 0 ; Head number
	xor bx, bx ; Data will be read to [ES:BX] = 0x07E0:0x0000 = 0x7E00
	int 0x13

	jc .read_error

	; Jump to stage 2
	jmp 0x0000:0x7E00

	cli
	hlt
	jmp $

.read_error:
	mov si, msg_read_error
	call print_string
	jmp $

%include "rmode_screen.asm"

msg_welcome db "Welcome to SimpleBootloader", 0xA, 0xD, 0
msg_read_error db "Could not read stage2!", 0

times 510 - ($ - $$) db 0
dw 0xAA55

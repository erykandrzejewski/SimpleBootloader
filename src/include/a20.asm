; If is locked, ZF is set
check_a20:
	; If a20 is locked, memory loops
	; 2 bytes on 0x7DFE equals 0xAA55 (boot signature)
	; So if 0xFFFF:0x7E0E equals 0xAA55 too, we can be sure that a20 is locked
	; 0xFFFF:0x7E0E = 0x0000:0x7DFE + 1MB

	push es

	mov bx, 0xFFFF
	mov es, bx

	mov bx, 0x7E0E

	cmp word [es:bx], 0xAA55

	pop es
	ret

unlock_a20_by_interrupt:
	mov ax, 0x2401
	int 0x15
	ret

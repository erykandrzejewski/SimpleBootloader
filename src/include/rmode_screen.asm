; Clear screen
clear_screen:
	xor ah, ah
	mov al, 3
	int 0x10
	ret

; Print character from AL register
print_char:
	mov ah, 0xE
	xor bh, bh
	mov bl, 0x7
	int 0x10
	ret

; Print string from SI register
print_string:
	lodsb
	or al, al
	jz .end
	call print_char
	jmp print_string
	.end:
		ret
